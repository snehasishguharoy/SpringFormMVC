/**
 * 
 */
package com.spring.web.form.controller;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.spring.web.domaim.Contact;

/**
 * @author sony
 *
 */
@Controller
public class FormController {

	@RequestMapping("/contacts")
	public ModelAndView showContacts() {
		return new ModelAndView("contact", "command", new Contact());

	}

	@RequestMapping(value = "/addContact", method = RequestMethod.POST)
	public String addContact(@ModelAttribute("contact") Contact contact, BindingResult bindingResult) {

		System.out.println("First Name is :" + contact.getFirstname() + " " + "Last Name is :" + contact.getLastname());
		return "redirect:contacts.html";

	}

}
